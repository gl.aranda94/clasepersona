
package cl.Aranda;

/**
 * @author Gloria Aranda
 */
public class Persona {
    //01 Crear los atributos de la clase
    private String rut;
    private String nombre;
    private int edad;
    
    //02 crear accesadores y mutadores(Getter y Setter)

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    //03 Crear constructores con o sin parametros.

    public Persona() {
        System.out.println("Holi soy el sin parametros");
    }

    public Persona(String rut, String nombre, int edad) {
        this.rut = rut;
        this.nombre = nombre;
        this.edad = edad;
        System.out.println("Holi soy el con parametros");
    }
    //04 crear metodo toString

    @Override
    public String toString() {
        return "Persona{" + "rut=" + rut + ", nombre=" + nombre + ", edad=" + edad + '}';
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
